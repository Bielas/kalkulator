import calc_hepler


def get_int(text):
    while True:
            try:
                return int(input(text))
            except ValueError:
                print("OOps! That was no valid number")

def get_float(text):
    while True:
        try:
            return float(input(text))
        except ValueError:
            print("Podaj liczbę!!!")


def pobierz_wartosc():
    a = get_float("Podaj pierwszą liczbę: ")
    b = get_float("Podaj drugą liczbę: ")
    return a, b

if __name__ == "__main__":
    while True:
        print("Wybierz z poniższej listy:")
        print("1: Dodaj")
        print("2: Odejmij")
        print("3: Pomnoż")
        print("4: Podziel")
        print("0: Wyjście")
        wybor = get_int("Twoj wybor: \n")
        if wybor == 0:
            break

        x, y = pobierz_wartosc()
        if wybor == 1:
            print("{} + {} = {}".format(x,y,calc_hepler.add(x,y)))
        elif wybor == 2:
            print("{} - {} = {}".format(x,y,calc_hepler.sub(x,y)))
        elif wybor == 3:
            print("{} * {} = {}".format(x,y,calc_hepler.multi(x,y)))
        elif wybor == 4:
            print("{} / {} = {}".format(x,y,calc_hepler.div(x,y)))